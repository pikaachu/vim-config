autocmd VimEnter *
  \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \|   PlugInstall --sync | q
  \| endif

call plug#begin('~/.local/share/nvim/plugged') 

Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
Plug 'junegunn/vim-easy-align'

""""""""
" THEMES
""""""""
" Breve
Plug 'AlessandroYorba/Breve'

" Duotone
Plug 'atelierbram/Base2Tone-vim'

" Ice Age
Plug 'sainnhe/vim-color-ice-age'

" Edge
Plug 'sainnhe/edge'

" Fairy Garden
Plug 'sainnhe/vim-color-fairy-garden'

" One Half
Plug 'sonph/onehalf', { 'rtp': 'vim/' }

" Awesome
Plug 'rafi/awesome-vim-colorschemes'

" Cake16
Plug 'zefei/cake16'

" Dracula
Plug 'dracula/vim', { 'as': 'dracula' }

" Pencil
Plug 'reedes/vim-colors-pencil'

" Paper Color
Plug 'NLKNguyen/papercolor-theme'

" Spacemacs
Plug 'liuchengxu/space-vim-dark'

" Ayu theme
Plug 'ayu-theme/ayu-vim'

" Base 16
Plug 'chriskempson/base16-vim'

" Gruvbox
Plug 'morhetz/gruvbox'

""""""""
" General coding tools
""""""""
" Lightline
Plug 'itchyny/lightline.vim'
Plug 'maximbaz/lightline-ale'

" NERDTree
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

" FZF
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" Autocompletion, very necessary
"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'neoclide/coc.nvim', {'tag': '*', 'branch': 'release'}

" Dispatch
Plug 'tpope/vim-dispatch'
Plug 'radenling/vim-dispatch-neovim'

" Linter
Plug 'w0rp/ale'

" Jump to
Plug 'philip-karlsson/aerojump.nvim', { 'do': ':UpdateRemotePlugins' }


" Make
Plug 'neomake/neomake'

" Tests
Plug 'janko-m/vim-test'

" Terminal/REPL
Plug 'kassio/neoterm'

" Commenting
Plug 'scrooloose/nerdcommenter'

" LSP
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }

" Targets
Plug 'wellle/targets.vim'

" Editorconfig
Plug 'editorconfig/editorconfig-vim'

" Git
Plug 'tpope/vim-fugitive'

" Projectionist
Plug 'tpope/vim-projectionist'

"""""""""""""""
" Languages
"""""""""""""""
"" Haskell
Plug 'neovimhaskell/haskell-vim'

"" Python
Plug 'deoplete-plugins/deoplete-jedi'



"" Polyglot
Plug 'sheerun/vim-polyglot'


"" Clojure
"Plug 'Olical/conjure', { 'tag': 'v0.23.0', 'do': 'bin/compile'  }
"Plug 'Olical/conjure', { 'tag': 'v0.18.0', 'do': 'bin/compile', 'for': 'clojure', 'on': 'ConjureUp'  }

Plug 'clojure-vim/acid.nvim', { 'do': ':UpdateRemotePlugins' }
"Plug 'guns/vim-clojure-static'
"Plug 'Vigemus/impromptu.nvim'
"Plug 'clojure-vim/async-clj-omni'
"Plug 'clojure-vim/clj-refactor.nvim'
"Plug 'clojure-vim/vim-jack-in'
"Plug 'clojure-vim/vim-cider'
"Plug 'clojure-vim/jazz.nvim'
"Plug 'tpope/vim-salve'
"Plug 'tpope/vim-fireplace'

"" ReasonML
Plug 'reasonml-editor/vim-reason-plus'


"" Rust
Plug 'rust-lang/rust.vim'

"" JS/TS
"Plug 'leafgarland/typescript-vim'
Plug 'HerringtonDarkholme/yats.vim'
Plug 'maxmellon/vim-jsx-pretty'
Plug 'mxw/vim-jsx'
Plug 'chemzqm/vim-jsx-improve'

"" Zig
Plug 'ziglang/zig.vim'

"" Dhall
Plug 'vmchale/dhall-vim'

"" CSS3
Plug 'hail2u/vim-css3-syntax'

"" Purescript
Plug 'frigoeu/psc-ide-vim'

call plug#end()

