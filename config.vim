let mapleader=" "
let maplocalleader=","

set tabstop=2 shiftwidth=2 expandtab

set termguicolors
let ayucolor="light"
set background=light
colorscheme edge

set number

nmap <Leader>sa <Plug>(AerojumpSpace)

"autocmd Syntax clojure EnableSyntaxExtension
let g:airline_theme = 'one'

let g:lightline = {
		\ 'colorscheme': 'edge'
		\ }
