let g:deoplete#enable_at_startup = 1

call deoplete#config(

let g:deoplete#deoplete_omni_patterns = get(g:, 'deoplete#force_omni_input_patterns', {})

let g:deoplete#sources = {}
let g:deoplete#sources._=['omni', 'buffer', 'member', 'tag', 'ultisnips', 'file']
"
"
au VimEnter * call deoplete#initialize()

"let g:deoplete#keywords = {}

