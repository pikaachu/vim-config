let g:coc_global_extensions = [
      \ 'coc-json', 'coc-python', 'coc-tsserver', 'coc-rls',
      \ 'coc-yaml', 'coc-git', 'coc-solargraph', 'coc-marketplace',
      \ 'coc-tslint-plugin', 'coc-eslint-plugin'
      \ ]
