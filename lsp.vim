set hidden

"let g:LanguageClient_devel = 1 " Use rust debug build
"let g:LanguageClient_loggingLevel = 'DEBUG' " Use highest logging level

let g:LanguageClient_autoStart = 1

let g:LanguageClient_serverCommands = {
    \ 'rust': ['rustup', 'run', 'nightly', 'rls'],
    \ 'python': ['/Users/arun/.local/bin/pyls'],
    \ 'reason': ['ocaml-language-server'],
    \ 'javascript': ['javascript-typescript-stdio'],
    \ 'javascript.jsx': ['javascript-typescript-stdio'],
    \ 'typescript': ['typescript-language-server', '--stdio'],
    \ 'typescript.jsx': ['typescript-language-server', '--stdio'],
    \ }

nnoremap <F5> :call LanguageClient_contextMenu()<CR>
